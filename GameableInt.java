package ejercicioscursojava;

public interface GameableInt {
	
	public void startGame();

	default void setGameName(String name) {
		System.out.println("GameName: "+name);
	}
}