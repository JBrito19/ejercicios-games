package ejercicioscursojava;

import java.util.Optional;

public interface PrincipalGame2 {
	// implements GameableInt, SoundableInt, PlayeableInt
	static String gameName=null;
    static double playerPosx=1.00; 
    static double playerPosY=2.00;
    
	public void startGame();
	
	public void walk(double x, double y);
	
	default void setGameName(String name) {
		System.out.println("GameName: "+Optional.ofNullable(name).orElseThrow(() -> {throw new RuntimeException("nameIsNULL");}));	
	}
	
	public void playMusic(String song);
    
    public static void main(String[] args) {	
	    /*
	    En el método walk(double x, double y) se debe de aumentar los
	    atributos double playerPosx double playerPosY en una unidad.
	    */
    	System.out.println("e1. \nplayerPosx: "+playerPosx+" playerPosY: "+playerPosY);
    	
    	PlayeableInt P = (playerPosx, playerPosY) -> {
    		playerPosx++; playerPosY++;	
    		System.out.println("playerPosx: "+playerPosx+" playerPosY: "+playerPosY);
    	};
	    P.walk(playerPosx, playerPosY);
	    
	    /*
	    En el método startGame() debe de imprimir el atributo gameName definido
	    en la clase usando el método por default de alguna de las interfaces.
	    */
	    GameableInt G =()->{
	    	P.setGameName(gameName);
	    };
	    System.out.println("e2.");
	    try{
	    	G.startGame();
	    }catch(Exception e){
	    	System.out.println("e3.");
	    	System.out.println("El nombre es null. Exception: "+e.getMessage());
	    }
	    
	    /*
	    En el método setGameName(String name), se debe de verificar con el
	    objeto Optional si el atributo gameName de la clase PrincipalName es
	    nulo, si lo es solamente setear dicho atributo con el argumento recibido en el
	    método, en caso contrario mandar una excepción con el mismo objeto
    	*/
	    
    
    }

}