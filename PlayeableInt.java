package ejercicioscursojava;

import java.util.Optional;

public interface PlayeableInt {
	
	
	public void walk(double x, double y);
	
	default void setGameName(String name) {
		
			System.out.println("GameName: "+Optional.ofNullable(name).orElseThrow(() -> {throw new RuntimeException("nameIsNULL");}));	
		
	}
}