package ejercicioscursojava;

public interface SoundableInt {
	
	public void playMusic(String song);
	
	default void setGameName(String name) {
		System.out.println("GameName: "+name);
	}

}