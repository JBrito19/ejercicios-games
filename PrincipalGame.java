package ejercicioscursojava;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;

import ejercicioscursojava.*;

public class PrincipalGame {
	// implements GameableInt, SoundableInt, PlayeableInt
	static String gameName="Juego";
    static double playerPosx=1.00; 
    static double playerPosY=2.00;
    
    public static void main(String[] args) {	
	    /*
	    En el método walk(double x, double y) se debe de aumentar los
	    atributos double playerPosx double playerPosY en una unidad.
	    */
    	System.out.println("e1. \nplayerPosx: "+playerPosx+" playerPosY: "+playerPosY);
    	
    	PlayeableInt P = (playerPosx, playerPosY) -> {
    		playerPosx++; playerPosY++;	
    		System.out.println("playerPosx: "+playerPosx+" playerPosY: "+playerPosY);
    	};
	    P.walk(playerPosx, playerPosY);
	    
	    /*
	    En el método startGame() debe de imprimir el atributo gameName definido
	    en la clase usando el método por default de alguna de las interfaces.
	    */
	    GameableInt G =()->{
	    	P.setGameName(gameName);
	    };
	    System.out.println("e2.");
	    try{
	    	G.startGame();
	    }catch(Exception e){
	    	System.out.println("e3.");
	    	System.out.println("El nombre es null. Exception: "+e.getMessage());
	    }
	    
	    List<Alumno> list=new ArrayList<Alumno>();  
	    Alumno alumno;
	    for (int i = 0; i < 20; i++) {
	    	alumno = new Alumno();
	    	alumno.setNombreCurso("nombreCurso"+i+"a");
	    	alumno.setNombre("nombre"+i+"a");
	    	alumno.setApellidoPaterno("apellidoPaterno"+i);
	    	alumno.setApellidoMaterno("apellidoMaterno"+i);
			list.add(alumno);
	    }
	   
	    list.forEach((Alumno a)->System.out.println(a));
	    
	    System.out.println("Imprimir tamaño de la lista: "+list.size());
	    
	    System.out.println("Imprimir con terminación a");
	    list.stream().filter(al -> al.getNombre().charAt(al.getNombre().length() - 1) == 'a').forEach(System.out::println);
	    
	    /*  
	 	list.stream().forEach(al -> {
	        if (al.getNombre().charAt(al.getNombre().length() - 1) == 'a') {
	        	System.out.println(al);  
	        }
	    });
	    */
	    System.out.println("Imprimir 5");
	    list.stream().limit(5).forEach(System.out::println);
	    
	    //list.forEach(System.out::println);
	    
	    /*
	    En el método setGameName(String name), se debe de verificar con el
	    objeto Optional si el atributo gameName de la clase PrincipalName es
	    nulo, si lo es solamente setear dicho atributo con el argumento recibido en el
	    método, en caso contrario mandar una excepción con el mismo objeto
    	*/
	    
    
    }

}