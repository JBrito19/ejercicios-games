package jdbc;
import java.sql.*;

public class Connecion {
	private Connection connection = null;
	
	public java.sql.Connection creaConexion()  {
		try {
			String url = "jdbc:sybase:Tds:";
			String serverName = "192.168.17.75";
			String portNumber = "5000";
			String sid = "SPEI";
			String usuariodb="sa";
			String passdb="enlace";
        	Class.forName("com.sybase.jdbc3.jdbc.SybDriver").newInstance();
        	if(connection == null || connection.isClosed()){
        		System.out.println("Creando conexion con BD...");
        		connection = DriverManager.getConnection( url + serverName + ":" + portNumber+ "/" + sid, usuariodb, passdb);
        	}
        } catch (Exception e) {
                e.printStackTrace();
        }
        return connection;
	}


	public void cierraConexion() {
        try {
                if (connection != null)
                        connection.close();
                connection = null;
        } catch (Exception e) {
                e.printStackTrace();
        }
	}
	
	public void displayDB() {
        java.sql.ResultSet rs = null;
        String statement = "select usu_clave, usu_nombre from bmx_usuarios";
        try {
                connection = this.creaConexion();
                if (connection != null) {
                        System.out.println("Conexion Exitosa");
                        Statement st = connection.createStatement();
                        rs = st.executeQuery(statement);
                        while (rs.next()) {
                        	System.out.println("Nombre: " + rs.getString(1)+" Cargo: " + rs.getString(2));
                        }

                        st.close();
         		       	st = null;
                        rs.close();
                        rs = null;
                        cierraConexion();
                } else
                	System.out.println("Error: No active Connection");
        } catch (Exception e) {
                e.printStackTrace();
        }
	}
	
	public static void main(String[] args) throws Exception {
        Connecion test = new Connecion();
        test.displayDB();
	}
	
}
