package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.Alumno;

public class AlumnoDAOImpl implements AlumnoDAO{
	String url = "jdbc:sybase:Tds:";
	String serverName = "192.168.17.75";
	String portNumber = "5000";
	String sid = "SPEI";
	String usuariodb="sa";
	String passdb="enlace";
	String consulta="select usu_clave, usu_nombre from bmx_usuarios";
	
	@Override
	public List<Alumno> getAlumnos() throws SQLException {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.sybase.jdbc3.jdbc.SybDriver").newInstance();
		} catch(Exception e) {
		    System.out.println("Error instanciando el Driver");
		    e.printStackTrace();
		}
        List<Alumno> al = new ArrayList<>();
        try ( Connection connection = DriverManager.getConnection( url + serverName + ":" + portNumber+ "/" + sid, usuariodb, passdb);
                PreparedStatement ps = connection.prepareStatement(consulta);
                ResultSet rs = ps.executeQuery()) {
        	Alumno alumno;
            while (rs.next()) {
            	alumno = new Alumno();
    	    	alumno.setNombreCurso(rs.getString(1));
    	    	alumno.setNombre(rs.getString(2));
                al.add(alumno);
            }
            return al;
        } catch (Exception e) {
            throw e;
        }
	}

}
