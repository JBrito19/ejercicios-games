package jdbc;

import java.sql.SQLException;
import java.util.List;

public interface AlumnoDAO {
	
	public List<Alumno> getAlumnos() throws SQLException;
}
