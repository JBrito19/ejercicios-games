package jdbc;

import java.io.Serializable;
 
public class Alumno implements Serializable {
	public static final long serialVersionUID = -123456789;
	
	
	private String nombre;
	private String nombreCurso;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNombreCurso() {
		return nombreCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	
	@Override
	public String toString(){
		return "Nombre: '" + this.nombre + "', Curso: '" + this.nombreCurso + "'";	
	}
	
}
