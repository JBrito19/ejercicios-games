package jdbc;

public class Main {
	public static void main(String[] args) {
        try {
            AlumnoDAO dao = AlumnoFactory.getInstance();
            dao.getAlumnos().forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
