package ejercicioscursojava;

public class Alumno {
	public static final long serialVersionUID = -123456789;
	
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombreCurso;
	private Long edad;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombreCurso() {
		return nombreCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	
	@Override
	public String toString(){
		return "Nombre: '" + this.nombre + "', apellidoPaterno: '" + this.apellidoPaterno + "', apellidoMaterno: '" + this.apellidoMaterno + "'"+ "', nombreCurso: '" + this.nombreCurso + "'";	
	}
	
	 public static void main(String[] args) {
		 String nombre="nombre";
		 System.out.println(nombre.toString());
	 }
	
}
